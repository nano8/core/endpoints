### nano/core/endpoints

[![release](https://img.shields.io/packagist/v/laylatichy/nano-core-endpoints?style=flat-square)](https://packagist.org/packages/laylatichy/nano-core-endpoints)
[![PHPStan](https://img.shields.io/badge/PHPStan-Level%20max-brightgreen.svg?style=flat&logo=php)](https://shields.io/#/)
[![Total Downloads](https://img.shields.io/packagist/dt/laylatichy/nano-core-endpoints?style=flat-square)](https://packagist.org/packages/laylatichy/nano-core-endpoints)

##### CI STATUS

-   dev [![pipeline status](https://gitlab.com/nano8/core/endpoints/badges/dev/pipeline.svg)](https://gitlab.com/nano8/core/endpoints/-/commits/dev)
-   master [![pipeline status](https://gitlab.com/nano8/core/endpoints/badges/master/pipeline.svg)](https://gitlab.com/nano8/core/endpoints/-/commits/master)
-   release [![pipeline status](https://gitlab.com/nano8/core/endpoints/badges/release/pipeline.svg)](https://gitlab.com/nano8/core/endpoints/-/commits/release)

#### Install

-   `composer require laylatichy/nano-core-endpoints`
